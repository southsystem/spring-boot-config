# Spring Boot Config #
Arquivos e ferramentas necessários para o Workshop

## Ferramentas
- [Java 8 - JDK](https://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven](https://maven.apache.org/download.cgi)
- [Eclipse](https://www.eclipse.org/downloads/)
- [Docker - Windows](https://docs.docker.com/docker-for-windows/install/) - [Docker - Linux](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
- [Robo 3T](https://robomongo.org/download)
- [Postman](https://www.getpostman.com/apps)


## Passos

1- Instalar JDK
	- Colocar o JAVA_HOME nas variáveis de ambiente
2- Instalar Maven - 
	- https://www.mkyong.com/maven/how-to-install-maven-in-windows/
	- Colocar as variáveis de ambiente do M2_HOME E MAVEN_HOME
	- Adicionar em PATH o valor %M2_HOME%\bin
3- Adicionar o .m2 na pasta do usuário
4- Instalar Docker
	- Carregar as imagens. "docker load < mongo.tar"
 	- Testar com "docker run hello-world"
5- Instalar Eclipse
6- Instalar Robo 3t
7- Instalar Postman